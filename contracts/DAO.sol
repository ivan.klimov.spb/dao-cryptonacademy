// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "hardhat/console.sol";

contract DAO {

    uint64 public minimumQuorum;
    address public chairPerson;
    uint64 public debatingPeriodDuration;
    address public voteToken;

    uint128 public proposalsLength;
    //proposal id => Proposal 
    //id start from 1
    mapping(uint128 => Proposal) _proposals;

    // proposal id => voter address => isVoted
    mapping(uint128 => mapping(address => bool)) votedProposals;

    mapping(address => Voter) _voters;
    // voter address => voted id => proposal id
    mapping(address => mapping(uint128 => uint128)) _votedProposalIds;

    struct Proposal {
        bool finished;
        uint64 endDate;
        uint64 countVoteAgree;
        uint64 countVoteDisagree;
        bytes callData;
        address recipient;
    }

    struct Voter {
        uint128 lengthVotedProposal;
        uint64 deposit;
    }

    constructor(address chairPerson_, address voteToken_, uint64 minimumQuorum_, uint64 debatingPeriodDuration_) {
        chairPerson = chairPerson_;
        voteToken = voteToken_;
        minimumQuorum = minimumQuorum_;
        debatingPeriodDuration = debatingPeriodDuration_;
    }

    modifier onlyChairPerson() {
        require(msg.sender == chairPerson, "DAO: Only chair person");
        _;
    }

    modifier proposalMustBeCreated(uint128 proposalId) {
        require(proposalId > 0 && proposalId <= proposalsLength, "DAO: this proposal has not been created");
        _;
    }

    function proposal(uint128 proposalId) public view proposalMustBeCreated(proposalId) returns(Proposal memory) {
        return _proposals[proposalId];
    }

    function voter(address voterAddress) public view returns(Voter memory) {
        return _voters[voterAddress];
    }

    function addProposal(bytes memory callData, address recipient) external onlyChairPerson() {
        proposalsLength++;
        uint128 curentVoteId = proposalsLength;
        uint64 currentTime = uint64(block.timestamp);
        Proposal storage currentProposal = _proposals[curentVoteId];
        currentProposal.callData = callData;
        currentProposal.recipient = recipient;
        currentProposal.endDate = currentTime + debatingPeriodDuration;
    }

    function deposit(uint64 amount) external {
        address sender = msg.sender;
        IERC20(voteToken).transferFrom(sender, address(this), amount);

        Voter storage currentVoter = _voters[sender];

        currentVoter.deposit += amount;
    }

    function vote(uint128 proposalId, bool agree) external proposalMustBeCreated(proposalId) {
        address sender = msg.sender;

        require(!votedProposals[proposalId][sender], "DAO: already voted for this proposal");
        Proposal storage currentProposal = _proposals[proposalId];

        uint64 currentTime = uint64(block.timestamp);
        require(currentTime < currentProposal.endDate, "DAO: time voting has ended");

        Voter storage currentVoter = _voters[sender];
        require(currentVoter.deposit > 0, "DAO: deposit must be greater than zero");

        if(agree) {
            currentProposal.countVoteAgree += currentVoter.deposit;
        } else {
            currentProposal.countVoteDisagree += currentVoter.deposit;
        }

        votedProposals[proposalId][sender] = true;

        _votedProposalIds[sender][currentVoter.lengthVotedProposal] = proposalId;
        currentVoter.lengthVotedProposal++;
    }

    function finishProposal(uint128 proposalId) external proposalMustBeCreated(proposalId) {
        Proposal storage currentProposal = _proposals[proposalId];
        require(!currentProposal.finished, "DAO: already finished");
        uint64 currentTime = uint64(block.timestamp);
        require(currentTime >= currentProposal.endDate, "DAO: time voting has not ended");

        currentProposal.finished = true;
        if(
            ((currentProposal.countVoteAgree + currentProposal.countVoteDisagree) >= minimumQuorum) && 
            currentProposal.countVoteAgree > currentProposal.countVoteDisagree
        ) {
            console.log("currentProposal.recipient %s", currentProposal.recipient);
            (bool success, ) = currentProposal.recipient.call{value:0 } (currentProposal.callData);
            require(success, "ERROR call function");
        }
    }

    function withdraw() external {
        address sender = msg.sender;

       Voter storage currentVoter =  _voters[sender];

        for(uint128 voteId = 0; voteId < currentVoter.lengthVotedProposal; voteId++) {
            uint128 proposalId = _votedProposalIds[sender][voteId];
            require(_proposals[proposalId].finished, "DAO: can't withdraw until will finished all your votes");
        }

        uint64 currentDeposit = currentVoter.deposit;
        currentVoter.deposit = 0;
        IERC20(voteToken).transfer(sender, currentDeposit);
        currentVoter.lengthVotedProposal = 0;
    }
}
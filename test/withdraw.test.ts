const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, DAO, ERC20DAOToken } from "../src/types";
import { BigNumber } from "ethers";
import { getTimestamp } from "./timeStamp";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 100;
const AGREE = true;
const DISAGREE = false;

describe("Withdraw ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let minimumQuorum: number;
  let voteToken: ERC20DAOToken;
  let voteToken2: ERC20DAOToken;
  let debatingPeriodDuration: number;
  let jsonAbi;
  let calldata: string;
  let recipient: string;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[5];
    minimumQuorum = 200;
    debatingPeriodDuration = 100;

    const factory = await ethers.getContractFactory("ERC20DAOToken");
    voteToken = await factory.deploy(NAME, SYMBOL);
    await voteToken.deployed();

    const factory2 = await ethers.getContractFactory("ERC20DAOToken");
    voteToken2 = await factory2.deploy(NAME, SYMBOL);
    await voteToken2.deployed();

    const factoryDAO = await ethers.getContractFactory("DAO");
    dao = await factoryDAO.deploy(
      chairPerson.address,
      voteToken.address,
      minimumQuorum,
      debatingPeriodDuration
    );
    await dao.deployed();

    await voteToken.grantRole(await voteToken.MINTER_ROLE(), dao.address);

    jsonAbi = [
      {
        inputs: [
          {
            internalType: "address",
            name: "to",
            type: "address",
          },
          {
            internalType: "uint256",
            name: "amount",
            type: "uint256",
          },
        ],
        name: "mint",
        outputs: [],
        stateMutability: "nonpayable",
        type: "function",
      },
    ];

    const iface = new ethers.utils.Interface(jsonAbi);
    calldata = iface.encodeFunctionData("mint", [
      accounts[1].address,
      MINT_AMOUNT,
    ]);
    recipient = voteToken.address;

    await voteToken.mint(accounts[1].address, MINT_AMOUNT);
    await voteToken.connect(accounts[1]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[1]).deposit(MINT_AMOUNT);
    await voteToken.mint(accounts[2].address, MINT_AMOUNT);
    await voteToken.connect(accounts[2]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[2]).deposit(MINT_AMOUNT);
    await voteToken.mint(accounts[3].address, MINT_AMOUNT);
    await voteToken.connect(accounts[3]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[3]).deposit(MINT_AMOUNT);

    await dao.connect(chairPerson).addProposal(calldata, recipient);
    await dao.connect(chairPerson).addProposal(calldata, recipient);
  });

  it("Can withdraw without voting and balanceOf tokens will increase", async function () {
    const balanceTokensBeforeWithdraw = (
      await voteToken.balanceOf(accounts[1].address)
    ).toNumber();

    await dao.connect(accounts[1]).withdraw();

    const balanceTokensAfterWithdraw = (
      await voteToken.balanceOf(accounts[1].address)
    ).toNumber();

    expect(balanceTokensAfterWithdraw - balanceTokensBeforeWithdraw).to.equal(
      MINT_AMOUNT
    );
  });

  it("Can withdraw without voting and deposit will be 0", async function () {
    await dao.connect(accounts[1]).withdraw();

    const deposit = (await dao.voter(accounts[1].address)).deposit.toNumber();

    expect(deposit).to.equal(0);
  });

  it("Can withdraw after proposal finished", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    await dao.finishProposal(1);
    await dao.connect(accounts[1]).withdraw();

    const deposit = (await dao.voter(accounts[1].address)).deposit.toNumber();

    expect(deposit).to.equal(0);
  });

  it("Can withdraw after 2 proposal finished", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    await dao.connect(accounts[1]).vote(2, DISAGREE);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    await dao.finishProposal(1);
    await dao.finishProposal(2);
    await dao.connect(accounts[1]).withdraw();

    const deposit = (await dao.voter(accounts[1].address)).deposit.toNumber();

    expect(deposit).to.equal(0);
  });

  describe("Reverted", function () {
    it("can't withdraw until will finished all your votes", async () => {
      await dao.connect(accounts[1]).vote(1, AGREE);
      await dao.connect(accounts[1]).vote(2, DISAGREE);

      const timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
      ]);

      await dao.finishProposal(1);

      await expect(dao.connect(accounts[1]).withdraw()).to.be.revertedWith(
        "DAO: can't withdraw until will finished all your votes"
      );
    });
  });
});

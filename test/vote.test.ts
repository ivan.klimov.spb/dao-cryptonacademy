const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, DAO, ERC20DAOToken } from "../src/types";
import { BigNumber } from "ethers";
import { getTimestamp } from "./timeStamp";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 100;
describe("Vote ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let minimumQuorum: number;
  let voteToken: ERC20DAOToken;
  let debatingPeriodDuration: number;
  let jsonAbi;
  let calldata: string;
  let recipient: string;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[5];
    minimumQuorum = 200;
    debatingPeriodDuration = 100;

    const factory = await ethers.getContractFactory("ERC20DAOToken");
    voteToken = await factory.deploy(NAME, SYMBOL);
    await voteToken.deployed();

    const factoryDAO = await ethers.getContractFactory("DAO");
    dao = await factoryDAO.deploy(
      chairPerson.address,
      voteToken.address,
      minimumQuorum,
      debatingPeriodDuration
    );
    await dao.deployed();

    await voteToken.grantRole(await voteToken.MINTER_ROLE(), dao.address);

    jsonAbi = [
      {
        inputs: [
          {
            internalType: "address",
            name: "to",
            type: "address",
          },
          {
            internalType: "uint256",
            name: "amount",
            type: "uint256",
          },
        ],
        name: "mint",
        outputs: [],
        stateMutability: "nonpayable",
        type: "function",
      },
    ];

    const iface = new ethers.utils.Interface(jsonAbi);
    calldata = iface.encodeFunctionData("mint", [
      accounts[1].address,
      MINT_AMOUNT,
    ]);
    recipient = voteToken.address;

    await voteToken.mint(accounts[1].address, MINT_AMOUNT * 2);
    await voteToken.connect(accounts[1]).approve(dao.address, MINT_AMOUNT * 2);
    await dao.connect(accounts[1]).deposit(MINT_AMOUNT);
    await voteToken.mint(accounts[2].address, MINT_AMOUNT);
    await voteToken.connect(accounts[2]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[2]).deposit(MINT_AMOUNT);

    await voteToken.mint(accounts[3].address, MINT_AMOUNT);
    await voteToken.connect(accounts[3]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[3]).deposit(MINT_AMOUNT);

    await dao.connect(chairPerson).addProposal(calldata, recipient);
  });

  it("Should voted agree eq 0 before vote", async function () {
    const agree = true;

    let proposal = await dao.proposal(1);

    expect(proposal.countVoteAgree).to.equal(0);
  });

  it("Should voted disagree eq 0 before vote", async function () {
    const agree = true;

    let proposal = await dao.proposal(1);

    expect(proposal.countVoteDisagree).to.equal(0);
  });

  it("Should voted agree and correct increase", async function () {
    const agree = true;

    await dao.connect(accounts[1]).vote(1, agree);

    let proposal = await dao.proposal(1);

    expect(proposal.countVoteAgree).to.equal(MINT_AMOUNT);
  });

  it("Should voted disagree and correct increase", async function () {
    const disagree = false;

    await dao.connect(accounts[1]).vote(1, disagree);

    let proposal = await dao.proposal(1);

    expect(proposal.countVoteDisagree).to.equal(MINT_AMOUNT);
  });

  it("After first vote will add increase deposit and vote for second proposal, but voting for first wil not changed from this account", async function () {
    const agree = true;

    await dao.connect(accounts[1]).vote(1, agree);

    let proposal = await dao.proposal(1);
    const proposaleVoteAgreeBeforeDeposit = proposal.countVoteAgree.toNumber();

    await dao.connect(chairPerson).addProposal(calldata, recipient);

    await dao.connect(accounts[1]).deposit(MINT_AMOUNT);
    await dao.connect(accounts[1]).vote(2, agree);

    const proposaleVoteAfterBeforeDeposit = proposal.countVoteAgree.toNumber();

    expect(
      proposaleVoteAfterBeforeDeposit - proposaleVoteAgreeBeforeDeposit
    ).to.equal(0);
  });

  it("After first vote will add increase deposit and vote for second proposal will be increase", async function () {
    const agree = true;

    await dao.connect(accounts[1]).vote(1, agree);
    await dao.connect(chairPerson).addProposal(calldata, recipient);

    await dao.connect(accounts[1]).deposit(MINT_AMOUNT);
    await dao.connect(accounts[1]).vote(2, agree);
    let proposal = await dao.proposal(2);
    const secondProposalResoltVote = await proposal.countVoteAgree.toNumber();

    expect(secondProposalResoltVote).to.equal(MINT_AMOUNT * 2);
  });

  describe("Reverted", function () {
    it("already voted for this proposal", async () => {
      const agree = true;
      await dao.connect(accounts[1]).vote(1, agree);

      await expect(dao.connect(accounts[1]).vote(1, agree)).to.be.revertedWith(
        "DAO: already voted for this proposal"
      );
    });

    it("time voting has ended", async () => {
      const agree = true;

      const timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
      ]);

      await expect(dao.connect(accounts[1]).vote(1, agree)).to.be.revertedWith(
        "DAO: time voting has ended"
      );
    });

    it("deposit must be greater than zero", async () => {
      const agree = true;
      await expect(dao.connect(accounts[4]).vote(1, agree)).to.be.revertedWith(
        "DAO: deposit must be greater than zero"
      );
    });
  });
});

const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, DAO, ERC20DAOToken } from "../src/types";
import { BigNumber } from "ethers";
import { getTimestamp } from "./timeStamp";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 100;
const AGREE = true;
const DISAGREE = false;

describe("FinishProposal ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let minimumQuorum: number;
  let voteToken: ERC20DAOToken;
  let voteToken2: ERC20DAOToken;
  let debatingPeriodDuration: number;
  let jsonAbi;
  let calldata: string;
  let recipient: string;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[5];
    minimumQuorum = 200;
    debatingPeriodDuration = 100;

    const factory = await ethers.getContractFactory("ERC20DAOToken");
    voteToken = await factory.deploy(NAME, SYMBOL);
    await voteToken.deployed();

    const factory2 = await ethers.getContractFactory("ERC20DAOToken");
    voteToken2 = await factory2.deploy(NAME, SYMBOL);
    await voteToken2.deployed();

    const factoryDAO = await ethers.getContractFactory("DAO");
    dao = await factoryDAO.deploy(
      chairPerson.address,
      voteToken.address,
      minimumQuorum,
      debatingPeriodDuration
    );
    await dao.deployed();

    await voteToken.grantRole(await voteToken.MINTER_ROLE(), dao.address);

    jsonAbi = [
      {
        inputs: [
          {
            internalType: "address",
            name: "to",
            type: "address",
          },
          {
            internalType: "uint256",
            name: "amount",
            type: "uint256",
          },
        ],
        name: "mint",
        outputs: [],
        stateMutability: "nonpayable",
        type: "function",
      },
    ];

    const iface = new ethers.utils.Interface(jsonAbi);
    calldata = iface.encodeFunctionData("mint", [
      accounts[1].address,
      MINT_AMOUNT,
    ]);
    recipient = voteToken.address;

    await voteToken.mint(accounts[1].address, MINT_AMOUNT);
    await voteToken.connect(accounts[1]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[1]).deposit(MINT_AMOUNT);
    await voteToken.mint(accounts[2].address, MINT_AMOUNT);
    await voteToken.connect(accounts[2]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[2]).deposit(MINT_AMOUNT);
    await voteToken.mint(accounts[3].address, MINT_AMOUNT);
    await voteToken.connect(accounts[3]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[3]).deposit(MINT_AMOUNT);

    await dao.connect(chairPerson).addProposal(calldata, recipient);
  });

  it("Should finised with agree and will do proposal(account[1] get ERC20 after mint)", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    await dao.connect(accounts[2]).vote(1, AGREE);
    await dao.connect(accounts[3]).vote(1, DISAGREE);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    const balanceERC20BeforeFinish = (
      await voteToken.balanceOf(accounts[1].address)
    ).toNumber();

    await dao.finishProposal(1);

    const balanceERC20AfterFinish = (
      await voteToken.balanceOf(accounts[1].address)
    ).toNumber();

    expect(balanceERC20AfterFinish - balanceERC20BeforeFinish).to.equal(
      MINT_AMOUNT
    );
  });

  it("Should finised and status will be finished)", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    await dao.connect(accounts[2]).vote(1, AGREE);
    await dao.connect(accounts[3]).vote(1, DISAGREE);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    await dao.finishProposal(1);

    const proposal = await dao.proposal(1);

    expect(proposal.finished).to.be.true;
  });

  it("Should finised with disagree and will do nothing)", async function () {
    await dao.connect(accounts[1]).vote(1, AGREE);
    await dao.connect(accounts[2]).vote(1, DISAGREE);
    await dao.connect(accounts[3]).vote(1, DISAGREE);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    const balanceERC20BeforeFinish = (
      await voteToken.balanceOf(accounts[1].address)
    ).toNumber();

    await dao.finishProposal(1);

    const balanceERC20AfterFinish = (
      await voteToken.balanceOf(accounts[1].address)
    ).toNumber();

    expect(balanceERC20AfterFinish - balanceERC20BeforeFinish).to.equal(0);
  });

  it("Should finised with not enough votes and will do nothing)", async function () {
    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
    ]);

    const balanceERC20BeforeFinish = (
      await voteToken.balanceOf(accounts[1].address)
    ).toNumber();

    await dao.finishProposal(1);

    const balanceERC20AfterFinish = (
      await voteToken.balanceOf(accounts[1].address)
    ).toNumber();

    expect(balanceERC20AfterFinish - balanceERC20BeforeFinish).to.equal(0);
  });

  describe("Reverted", function () {
    it("already finished", async () => {
      const timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
      ]);

      await dao.finishProposal(1);

      await expect(dao.finishProposal(1)).to.be.revertedWith(
        "DAO: already finished"
      );
    });

    it("time voting has not ended", async () => {
      await expect(dao.finishProposal(1)).to.be.revertedWith(
        "DAO: time voting has not ended"
      );
    });

    it("ERROR call function", async () => {
      await dao.connect(chairPerson).addProposal(calldata, voteToken2.address);

      await dao.connect(accounts[1]).vote(2, AGREE);
      await dao.connect(accounts[2]).vote(2, AGREE);
      await dao.connect(accounts[3]).vote(2, DISAGREE);

      const timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
      ]);

      await expect(dao.finishProposal(2)).to.be.revertedWith(
        "ERROR call function"
      );
    });
  });
});

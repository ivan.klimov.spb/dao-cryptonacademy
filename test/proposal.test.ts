const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, DAO, ERC20DAOToken } from "../src/types";
import { BigNumber } from "ethers";
import { getTimestamp } from "./timeStamp";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 100;
describe("Proposal ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let minimumQuorum: number;
  let voteToken: ERC20DAOToken;
  let debatingPeriodDuration: number;
  let jsonAbi;
  let calldata: string;
  let recipient: string;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[5];
    minimumQuorum = 200;
    debatingPeriodDuration = 100;

    const factory = await ethers.getContractFactory("ERC20DAOToken");
    voteToken = await factory.deploy(NAME, SYMBOL);
    await voteToken.deployed();

    const factoryDAO = await ethers.getContractFactory("DAO");
    dao = await factoryDAO.deploy(
      chairPerson.address,
      voteToken.address,
      minimumQuorum,
      debatingPeriodDuration
    );
    await dao.deployed();

    await voteToken.grantRole(await voteToken.MINTER_ROLE(), dao.address);

    jsonAbi = [
      {
        inputs: [
          {
            internalType: "address",
            name: "to",
            type: "address",
          },
          {
            internalType: "uint256",
            name: "amount",
            type: "uint256",
          },
        ],
        name: "mint",
        outputs: [],
        stateMutability: "nonpayable",
        type: "function",
      },
    ];

    const iface = new ethers.utils.Interface(jsonAbi);
    calldata = iface.encodeFunctionData("mint", [
      accounts[1].address,
      MINT_AMOUNT,
    ]);
    recipient = voteToken.address;
  });

  it("Should added proposal and correct calldata", async function () {
    await dao.connect(chairPerson).addProposal(calldata, recipient);

    var proposal = await dao.proposal(1);

    expect(proposal.callData).to.equal(calldata);
  });

  it("Should added proposal and correct recipient", async function () {
    await dao.connect(chairPerson).addProposal(calldata, recipient);

    var proposal = await dao.proposal(1);

    expect(proposal.recipient).to.equal(recipient);
  });

  it("Should added proposal and correct endDate", async function () {
    await dao.connect(chairPerson).addProposal(calldata, recipient);

    var proposal = await dao.proposal(1);
    let timeStamp = await getTimestamp();

    expect(proposal.endDate).to.equal(timeStamp + debatingPeriodDuration);
  });

  it("Should proposal length has increased", async function () {
    const proposalsLengthBeforeAdding = await dao.proposalsLength();

    await dao.connect(chairPerson).addProposal(calldata, recipient);

    const proposalsLengthAfterAdding = await dao.proposalsLength();

    expect(
      proposalsLengthAfterAdding.toNumber() -
        proposalsLengthBeforeAdding.toNumber()
    ).to.equal(1);
  });

  describe("Reverted", function () {
    it("Only chair person can add proposal", async () => {
      await expect(
        dao.connect(accounts[1]).addProposal(calldata, recipient)
      ).to.be.revertedWith("DAO: Only chair person");
    });

    it("can't get 0 proposal has not been created", async () => {
      await expect(dao.proposal(0)).to.be.revertedWith(
        "DAO: this proposal has not been created"
      );
    });

    it("can't get proposal has not been created", async () => {
      await dao.connect(chairPerson).addProposal(calldata, recipient);
      await expect(dao.proposal(2)).to.be.revertedWith(
        "DAO: this proposal has not been created"
      );
    });
  });
});

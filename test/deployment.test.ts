const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, DAO, ERC20DAOToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 100;
describe("Deployment ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let minimumQuorum: number;
  let voteToken: ERC20DAOToken;
  let debatingPeriodDuration: number;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[5];
    minimumQuorum = 200;
    debatingPeriodDuration = 100;

    const factory = await ethers.getContractFactory("ERC20DAOToken");
    voteToken = await factory.deploy(NAME, SYMBOL);
    await voteToken.deployed();
  });

  describe("Deployment", function () {
    it("Should set the right chairPerson", async function () {
      const factoryDAO = await ethers.getContractFactory("DAO");
      dao = await factoryDAO.deploy(
        chairPerson.address,
        voteToken.address,
        minimumQuorum,
        debatingPeriodDuration
      );
      await dao.deployed();

      expect(await dao.chairPerson()).to.equal(chairPerson.address);
    });

    it("Should set the right minimumQuorum", async function () {
      const factoryDAO = await ethers.getContractFactory("DAO");
      dao = await factoryDAO.deploy(
        chairPerson.address,
        voteToken.address,
        minimumQuorum,
        debatingPeriodDuration
      );
      await dao.deployed();

      expect(await dao.minimumQuorum()).to.equal(minimumQuorum);
    });

    it("Should set the right voteToken", async function () {
      const factoryDAO = await ethers.getContractFactory("DAO");
      dao = await factoryDAO.deploy(
        chairPerson.address,
        voteToken.address,
        minimumQuorum,
        debatingPeriodDuration
      );
      await dao.deployed();

      expect(await dao.voteToken()).to.equal(voteToken.address);
    });

    it("Should set the right minimumQuorum", async function () {
      const factoryDAO = await ethers.getContractFactory("DAO");
      dao = await factoryDAO.deploy(
        chairPerson.address,
        voteToken.address,
        minimumQuorum,
        debatingPeriodDuration
      );
      await dao.deployed();

      expect(await dao.debatingPeriodDuration()).to.equal(
        debatingPeriodDuration
      );
    });
  });
});

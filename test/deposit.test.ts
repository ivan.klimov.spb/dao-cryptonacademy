const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, DAO, ERC20DAOToken } from "../src/types";
import { BigNumber } from "ethers";
import { getTimestamp } from "./timeStamp";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 100;
describe("Deposit ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let minimumQuorum: number;
  let voteToken: ERC20DAOToken;
  let debatingPeriodDuration: number;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[5];
    minimumQuorum = 200;
    debatingPeriodDuration = 100;

    const factory = await ethers.getContractFactory("ERC20DAOToken");
    voteToken = await factory.deploy(NAME, SYMBOL);
    await voteToken.deployed();

    const factoryDAO = await ethers.getContractFactory("DAO");
    dao = await factoryDAO.deploy(
      chairPerson.address,
      voteToken.address,
      minimumQuorum,
      debatingPeriodDuration
    );
    await dao.deployed();

    await voteToken.grantRole(await voteToken.MINTER_ROLE(), dao.address);

    await voteToken.mint(accounts[1].address, MINT_AMOUNT * 2);
  });

  it("Should increase deposit", async function () {
    const voterDepositBeforeAddDeposit = (await dao.voter(accounts[1].address))
      .deposit;

    await voteToken.connect(accounts[1]).approve(dao.address, MINT_AMOUNT);
    await dao.connect(accounts[1]).deposit(MINT_AMOUNT);

    const voterDepositAfterAddDeposit = (await dao.voter(accounts[1].address))
      .deposit;

    expect(
      voterDepositAfterAddDeposit.toNumber() -
        voterDepositBeforeAddDeposit.toNumber()
    ).to.equal(MINT_AMOUNT);
  });

  it("Double increase deposit", async function () {
    const voterDepositBeforeAddDeposit = (await dao.voter(accounts[1].address))
      .deposit;

    await voteToken.connect(accounts[1]).approve(dao.address, MINT_AMOUNT * 2);
    await dao.connect(accounts[1]).deposit(MINT_AMOUNT);
    await dao.connect(accounts[1]).deposit(MINT_AMOUNT);

    const voterDepositAfterAddDeposit = (await dao.voter(accounts[1].address))
      .deposit;

    expect(
      voterDepositAfterAddDeposit.toNumber() -
        voterDepositBeforeAddDeposit.toNumber()
    ).to.equal(MINT_AMOUNT * 2);
  });
});

export const TASK_DEPOSIT = "deposit";
export const TASK_ADD_PROPOSAL = "addproposal";
export const TASK_VOTE = "vote";
export const TASK_FINISH = "finish";
export const TASK_WITHDRAW = "withdraw";
export const TASK_APPROVE = "approve";
export const TASK_GIVE_MINT_PERMISSION = "addMintPermission";

import { task } from "hardhat/config";
import { TASK_APPROVE } from "./task-names";

task(TASK_APPROVE, "Give mint permission")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("minter", "address account that can mint")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let token = await hre.ethers.getContractAt(
      "ERC20PresetMinterPauser",
      args.contract
    );

    const minterRole = await token.connect(account).MINTER_ROLE();

    await token.connect(account).grantRole(minterRole, args.minter);

    console.log("task give mint permission finished");
  });

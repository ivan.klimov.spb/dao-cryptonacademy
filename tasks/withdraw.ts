import { task } from "hardhat/config";
import { TASK_WITHDRAW } from "./task-names";

task(TASK_WITHDRAW, "Withdraw tokens")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let dao = await hre.ethers.getContractAt("DAO", args.contract);

    await dao.connect(account).withdraw();

    console.log("task withdraw finished");
  });

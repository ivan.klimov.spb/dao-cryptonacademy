import { task } from "hardhat/config";
import { TASK_DEPOSIT } from "./task-names";

task(TASK_DEPOSIT, "Deposit tokens")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("amount", "Amount of tokens")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let dao = await hre.ethers.getContractAt("DAO", args.contract);

    await dao.connect(account).deposit(args.amount);

    console.log("task deposit finished");
  });

#Do this first

Add your file .env to root project

Structure .env

```
PRIVATE_KEY = string
PRIVATE_KEY = string
PUBLIC_KEY = string
PRIVATE_KEY2 = string
PUBLIC_KEY2 = string
PRIVATE_KEY3 = string
PUBLIC_KEY3 = string
API_URL_RINKEBY = string
API_URL_GOERLI = string
API_KEY_ETHERSCAN = string
API_KEY_BSCSCAN = string
CHAIN_ID_FIRST = string
CHAIN_ID_SECOND = string
MINIMUM_QUORUM = string
DEBATING_PERIOD_DURATION = string
VOTE_TOKEN = string
```

#Start

```
npm install
npx hardhat compile
```

#Run test

```
npx hardhat coverage
```

#Deploy contract to rinkeby 
ERC20
```
npx hardhat run ./scripts/deployERC20Token.ts --network rinkeby
```
DAO
```
npx hardhat run ./scripts/deployDAO.ts --network rinkeby
```

#Verify contract

```
npx hardhat verify --network rinkeby --contract contracts/ERC20DAOToken.sol:ERC20DAOToken CONTRACT_ADDRESS NAME SYMBOL 
```

```
npx hardhat verify --network rinkeby CONTRACT_ADDRESS PUBLIC_KEY VOTE_TOKEN MINIMUM_QUORUM DEBATING_PERIOD_DURATION
```

#Look at tasks list

```
npx hardhat  --help
```

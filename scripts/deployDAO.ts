import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { DAO } from "../src/types";
import { BigNumber } from "ethers";

const { PUBLIC_KEY, VOTE_TOKEN, MINIMUM_QUORUM, DEBATING_PERIOD_DURATION } =
  process.env;

async function main() {
  const factory = await ethers.getContractFactory("DAO");
  console.log("Deploying DAO...");

  const contract: DAO = await factory.deploy(
    PUBLIC_KEY as string,
    VOTE_TOKEN as string,
    BigNumber.from(MINIMUM_QUORUM),
    BigNumber.from(DEBATING_PERIOD_DURATION)
  );

  await contract.deployed();

  console.log("DAO deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });

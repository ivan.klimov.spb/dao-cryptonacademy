import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { ERC20DAOToken } from "../src/types";

async function main() {
  const factory = await ethers.getContractFactory("ERC20DAOToken");
  console.log("Deploying ERC20DAOToken...");

  const contract: ERC20DAOToken = await factory.deploy("IVKLIM", "KLIM");

  await contract.deployed();

  console.log("ERC20DAOToken deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
